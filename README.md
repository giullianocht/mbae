# ¿ MBA'E ?

Es un proyecto realizado en Flutter que se desarrolló para el concurso de aplicaciones móviles de la XVII Exposición, Tecnológica y Científica “Ñamopu'ã Ñane Ñe'ê” – 2019 de la Facultad Politécnica de la Universidad Nacional de Asunción.


Es un juego para dos o más personas que buscan divertirse y aprender palabras cotidianas en guaraní, consiste en adivinar la palabra mediante mímicas o descripciones teniendo un tiempo limitado para adivinar todas las palabras posibles.

Si quieres probarlo puedes descargarlo con el siguiente link - [APK](https://drive.google.com/file/d/13tPavkOYm4HwBcKJELehp0ciIxUe91Wd/view)